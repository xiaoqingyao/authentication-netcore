﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authentication_cookie.Models
{
    public class UserStore
    {
        private static List<User> _users = new List<User>() {
        new User {  Id=1, Name="kakaxi", Password="kakaxi", Email="alice@gmail.com", PhoneNumber="18800000001" },
        new User {  Id=1, Name="kai", Password="kai", Email="bob@gmail.com", PhoneNumber="18800000002" }
    };

        public User FindUser(string userName, string password)
        {
            return _users.FirstOrDefault(_ => _.Name == userName && _.Password == password);
        }
    }
}
