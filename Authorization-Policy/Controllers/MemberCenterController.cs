﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authorization_Policy.Controllers
{
    //[Authorize(Policy = "MemberOnly")]
    public class MemberCenterController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Policy = "Over18")]
        public IActionResult Adult()
        {
            return View();
        }

        [Authorize(Policy ="Anbu")]
        public IActionResult Anbu() {
            return View();
        }
    }
}
