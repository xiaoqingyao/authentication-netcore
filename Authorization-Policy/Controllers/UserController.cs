﻿using Authentication_cookie.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using System.Text;
using IdentityModel;

namespace Authentication_cookie.Controllers
{
    public class UserController : Controller
    {
        private UserStore _userStore;
        private IHttpContextAccessor _httpcontext;
        public UserController(UserStore userStore, IHttpContextAccessor httpContextAccessor)
        {
            _userStore = userStore;
            _httpcontext = httpContextAccessor;
        }
        /// <summary>
        /// 用户首页
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            var IsAuthenticated = _httpcontext.HttpContext.User?.Identity?.IsAuthenticated ?? false;
            if (IsAuthenticated)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"当前登录用户：{_httpcontext.HttpContext.User.Identity.Name}<br/>");
                sb.Append($"验证类型：{_httpcontext.HttpContext.User.Identity.AuthenticationType}<br/>");
                foreach (var item in _httpcontext.HttpContext.User.Claims)
                {
                    sb.Append($"{item.Type}-{item.Value}<br/>");
                }
                ViewBag.UserMessage = sb.ToString();
            }
            ViewBag.IsAuthenticated = IsAuthenticated;
            return View();
        }
        /// <summary>
        /// 登录页
        /// </summary>
        /// <param name="ErrorMessage"></param>
        /// <returns></returns>
        public IActionResult Login(string ErrorMessage)
        {
            ViewBag.ErrorMessage = ErrorMessage;
            return View();
        }
        /// <summary>
        /// 登录验证
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Login(string Name, string Password)
        {
            var user = _userStore.FindUser(Name, Password);
            if (user == null)
            {
                return RedirectToAction("Login", new { ErrorMessage = "用户名密码不正确" });
            }
            else
            {
                var claimIdentity = new ClaimsIdentity("Cookie");
                claimIdentity.AddClaim(new Claim(JwtClaimTypes.Id, user.Id.ToString()));
                claimIdentity.AddClaim(new Claim(JwtClaimTypes.Name, user.Name));
                claimIdentity.AddClaim(new Claim(JwtClaimTypes.Email, user.Email));
                claimIdentity.AddClaim(new Claim(JwtClaimTypes.PhoneNumber, user.PhoneNumber));
                claimIdentity.AddClaim(new Claim(JwtClaimTypes.BirthDate, user.Birthday.ToString()));
                claimIdentity.AddClaim(new Claim(JwtClaimTypes.Address, user.Address.ToString()));
                claimIdentity.AddClaim(new Claim(JwtClaimTypes.Subject, user.Subject.ToString()));

                var claimsPrincipal = new ClaimsPrincipal(claimIdentity);
                HttpContext.SignInAsync(claimsPrincipal);
                return RedirectToAction("Index");
            }
        }
        /// <summary>
        /// 退出
        /// </summary>
        /// <returns></returns>
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return Redirect("Index");
        }
    }
}
