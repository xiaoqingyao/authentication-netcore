﻿using IdentityModel;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authorization_Policy.CustomAuthorizationHandlers
{
    /// <summary>
    /// 进入暗部的验证
    /// </summary>
    public class AnBuEnterRequirement : IAuthorizationRequirement
    {
    }
    /// <summary>
    /// 区域验证
    /// </summary>
    public class AreaHandler : AuthorizationHandler<AnBuEnterRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AnBuEnterRequirement requirement)
        {
            if (context.User != null && context.User.HasClaim(c => c.Type == JwtClaimTypes.Address))
            {
                var Address = context.User.FindFirst(c => c.Type == JwtClaimTypes.Address).Value;
                if (Address == "木叶")
                {
                    context.Succeed(requirement);
                }
            }
            return Task.CompletedTask;
        }
    }
    /// <summary>
    /// 主题验证
    /// </summary>
    public class SubjectHandler : AuthorizationHandler<AnBuEnterRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AnBuEnterRequirement requirement)
        {
            if (context.User != null && context.User.HasClaim(c => c.Type == JwtClaimTypes.Subject))
            {
                var Subject = context.User.FindFirst(c => c.Type == JwtClaimTypes.Subject).Value;
                if (Subject == "雷切")
                {
                    context.Succeed(requirement);
                }
            }
            return Task.CompletedTask;
        }
    }

}
