﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authentication_cookie.Models
{
    public class UserStore
    {
        private static List<User> _users = new List<User>() {
        new User {  Id=1, Name="kakaxi", Password="kakaxi", Email="alice@qq.com", PhoneNumber="15100000001", Birthday=DateTime.Parse("2007-01-01"),Address="木叶",Subject="雷切" },
        new User {  Id=2, Name="kai", Password="kai", Email="bob@163.com", PhoneNumber="18600000002" , Birthday=DateTime.Parse("1991-01-01"),Address="不知道", Subject="体术" }
    };

        public User FindUser(string userName, string password)
        {
            return _users.FirstOrDefault(_ => _.Name == userName && _.Password == password);
        }
    }
}
