using Authentication_cookie.Models;
using Authorization_Policy.CustomAuthorizationHandlers;
using Authorization_Policy.CustomRequirements;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authorization_Policy
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<UserStore>();

            //多handler授权策略 DI
            services.AddSingleton<IAuthorizationHandler, AreaHandler>();
            services.AddSingleton<IAuthorizationHandler, SubjectHandler>();


            //添加认证
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            }).AddCookie(options =>
            {
                //cookie认证更多配置
                options.Cookie.Name = "AuthCookie";//cookie名称
                options.LoginPath = "/User/Login";//登录路径
                options.Cookie.HttpOnly = true;//cookie操作权限
                options.SessionStore = new MemoryCacheTicketStore();
            });
            //添加授权策略
            var commonPolicy = new AuthorizationPolicyBuilder().RequireClaim("MyType").Build();

            services.AddAuthorization(options =>
            {
                //会员身份验证
                options.AddPolicy("MemberOnly", p =>
                {
                    //p.RequireClaim("MemberCardCode");
                    p.RequireClaim("Name");//必须包含某个Claim项
                });

                //必须包含某个Claim项
                options.AddPolicy("User", policy => policy
                    .RequireAssertion(context => context.User.HasClaim(c => (c.Type == "EmployeeNumber" || c.Type == "Role")))
                );
                //综合控制
                options.AddPolicy("Employee", policy => policy
                        .RequireRole("Admin")//角色
                        .RequireUserName("Alice")//身份验证
                        .RequireClaim("EmployeeNumber")//必须包含某个Claim项
                        .Combine(commonPolicy));//合并其他策略
                //自定义策略
                options.AddPolicy("Over18", p => p.Requirements.Add(new MinimumAgeRequirement(18)));
                //多Handler验证
                options.AddPolicy("Anbu", p => p.Requirements.Add(new AnBuEnterRequirement()));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
