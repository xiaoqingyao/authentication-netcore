﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JWTDemo.Authentication
{
    public class UserStore
    {
        private List<UserModel> _users = new List<UserModel>() {
            new UserModel(){ Id=1, UserName="kakaxi", Password="kakaxi", Email="a@a.com", PhoneNumber="1234" }
        };
        public UserModel FindUser(string name, string pwd)
        {
            return _users.FirstOrDefault(p => p.UserName == name && p.Password == pwd);
        }
    }
}
